FROM openjdk:8
COPY target/sprintboot-docker-web.jar .
EXPOSE 8080
CMD ["java","-jar","sprintboot-docker-web.jar"]